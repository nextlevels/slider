<?php return [
    'plugin' => [
        'name' => 'Slider',
        'description' => ''
    ],
    'models' => [
        'slider' => [
            'fields' => [
                'name' => 'Name',
                'settings_autoplay' => 'Autostart',
                'settings_duration' => 'Dauer'
            ],
            'tabs' => [
                'settings' => 'Einstellungen',
                'slides' => 'Slides'
            ]
        ],
        'slider_item' => [
            'fields' => [
                'title' => 'Titel',
                'link' => 'Link',
                'picture' => 'Bild',
                'sort_order' => 'Sortierung',
                'orientation' => 'Orientierung',
                'sub_headline' => 'Untertitel'
            ],
            'tabs' => [
                'settings' => 'Einstellungen',
                'slides' => 'Slides'
            ]
        ]
    ],
    'components' => [
        'sliderview' => [
            'name' => 'Slider View Komponente',
            'description' => 'Erzeugt Slider Views',
        ]
    ]
];

<?php namespace Nextlevels\Slider\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNextlevelsSliderSliderItems2 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_slider_slider_items', function($table)
        {
            $table->text('title')->nullable(false)->unsigned(false)->default(null)->change();
            $table->text('sub_headline')->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('nextlevels_slider_slider_items', function($table)
        {
            $table->string('title', 191)->nullable(false)->unsigned(false)->default(null)->change();
            $table->string('sub_headline', 191)->nullable()->unsigned(false)->default(null)->change();
        });
    }
}

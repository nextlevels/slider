<?php namespace Nextlevels\Slider\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNextlevelsSliderSliderItems extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_slider_slider_items', function($table)
        {
            $table->integer('slider_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('nextlevels_slider_slider_items', function($table)
        {
            $table->integer('slider_id')->nullable(false)->change();
        });
    }
}

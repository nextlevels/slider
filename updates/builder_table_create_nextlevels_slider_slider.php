<?php namespace Nextlevels\Slider\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNextlevelsSliderSlider extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_slider_slider', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->boolean('settings_autoplay')->default(0);
            $table->integer('settings_duration')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nextlevels_slider_slider');
    }
}

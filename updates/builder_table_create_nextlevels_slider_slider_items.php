<?php namespace Nextlevels\Slider\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNextlevelsSliderSliderItems extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_slider_slider_items', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->string('link')->nullable();
            $table->string('picture');
            $table->integer('sort_order')->nullable();
            $table->string('orientation');
            $table->string('sub_headline')->nullable();
            $table->integer('slider_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nextlevels_slider_slider_items');
    }
}

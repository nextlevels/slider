<?php namespace Nextlevels\Slider\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNextlevelsSliderSliders2 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_slider_sliders', function($table)
        {
            $table->integer('settings_duration')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('nextlevels_slider_sliders', function($table)
        {
            $table->integer('settings_duration')->default(null)->change();
        });
    }
}

<?php namespace Nextlevels\Slider\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNextlevelsSliderSliders extends Migration
{
    public function up()
    {
        Schema::rename('nextlevels_slider_slider', 'nextlevels_slider_sliders');
    }
    
    public function down()
    {
        Schema::rename('nextlevels_slider_sliders', 'nextlevels_slider_slider');
    }
}

<?php namespace Nextlevels\Slider\Components;

use Cms\Classes\ComponentBase;
use Nextlevels\Slider\Models\Slider;

/**
 * Class PartnerLinksList
 *
 */
class SliderView extends ComponentBase
{
    /**
     * @var Slider
     */
    protected $slider;

    /**
     * Set slider
     *
     * @return void
     */
    protected function getSlider()
    {
        return Slider::first();
    }

    /**
     * @return array
     */
    public function componentDetails(): array
    {
        return [
            'name'        => 'nextlevels.slider::lang.plugin.components.sliderview.name',
            'description' => 'nextlevels.slider::lang.plugin.components.sliderview.description'
        ];
    }

    /**
     * On run
     */
    public function onRun(): void
    {
        if ($sliderName = $this->property('slider')) {
            $slider = Slider::where('name', $sliderName)->with('slider_items')->first();
            $this->page['slider'] = $slider;
            return;
        }

        $this->page['slider_items'] = $this->getSlider()->slider_items->sortBy('sort_order')->all();
        $this->page['slider'] = $this->slider = $this->getSlider();
    }
}

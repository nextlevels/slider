<?php namespace Nextlevels\Slider;

use System\Classes\PluginBase;
use Nextlevels\Slider\Components\SliderView;

/**
* Class Plugin
*
* @author Jan Malte Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
*/
class Plugin extends PluginBase
{
    /**
     * Register Components 
     *
     * @return void
     */
    public function registerComponents()
    {
        return [
            SliderView::class => 'SliderView',
        ];
    }
}

<?php namespace Nextlevels\Slider\Controllers;

use Backend\Behaviors\FormController;
use Backend\Behaviors\ListController;
use Backend\Behaviors\RelationController;
use Backend\Classes\Controller;
use BackendMenu;

/**
* Class Sliders
*
* @author Jan Malte Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
*/
class Sliders extends Controller
{
    public $implement = [FormController::class, ListController::class, RelationController::class];

    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Nextlevels.Slider', 'main-menu-item');
    }
}

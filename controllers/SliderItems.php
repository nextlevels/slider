<?php namespace Nextlevels\Slider\Controllers;

use Backend\Classes\Controller;
use Backend\Behaviors\ListController;
use Backend\Behaviors\FormController;

/**
* Class SliderItems
*
* @author Jan Malte Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
*/
class SliderItems extends Controller
{
    public $implement = [ListController::class,FormController::class];

    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }
}

<?php namespace Nextlevels\Slider\Models;

use Model;
use October\Rain\Database\Traits\Validation;

/**
* Class Slider
*
* @author Jan Malte Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
*/
class Slider extends Model
{
    use Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_slider_sliders';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = ['slider_items' => [SliderItem::class]];
}
